package fr.cnam.foad.nfa035.fileutils.simpleaccess;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Base64;

/**
 * Classe de serialisation d'image et désérialisation
 */

public class ImageSerializerBase64Impl implements ImageSerializer {
	
	/**
	 * La classe sert à la sérialisation et désérialisation d'une image
	 */
	
	@Override
	public String serialize(File image) {
		/**
		 * Essaie d'ouvrir et lire un fichier image prit en attribut, puis lit les bytes constituant le fichier avant de les convertir en Base64, puis retourne au format String
		 * 
		 * @param image un fichier image
		 */
		FileInputStream imageStream;
		String encodedImage = null;
		try {
			imageStream = new FileInputStream(image);
			
	        byte[] data = imageStream.readAllBytes();
			
			encodedImage = Base64.getEncoder().encodeToString(data);
			

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return encodedImage;
	}

	@Override
	public byte[] deserialize(String encodedImage) {
		/**
		 *  Prend en attribut un String en Base64 et le convertit en bytes. Retourne les bytes sous la forme d'un tableau au format byte[]
		 *  
		 * @param encodedImage suite de caractères issue de la conversion d'une image de bytes vers Base64
		 */
		byte[] data = null;

		data = Base64.getDecoder().decode(encodedImage);
			
		return data;
	}
	

}
